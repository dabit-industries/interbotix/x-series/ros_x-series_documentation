site_name: Interbotix X-Series Documentation
site_description: Trossen Robotics - Interbotix X-Series Documentation
site_author: Dabit Industries, LLC
site_url: https://dabit-industries.gitlab.io/interbotix/x-series/ros_x-series_documentation/

repo_name: x-series/ros_x-series_documentation
repo_url: https://gitlab.com/dabit-industries/interbotix/x-series/ros_x-series_documentation

copyright: Copyright &copy;2019 Dabit Industries, LLC

theme:
    name: material
    logo: 'assets/img/Interbotix_Logo.svg'
    favicon: 'assets/img/Interbotix_Logo_bg.png'
    language: en
    palette:
        primary: 'Blue'
        accent: 'Red'
    font:
        text: Roboto
        code: Roboto Mono
    feature:
        tabs: false
    custom_dir: 'theme'

extra_css:
    - 'assets/css/extra.css'

# For more information, check out:
#     https://facelessuser.github.io/pymdown-extensions/
markdown_extensions:
    - admonition  # block-style content
    - codehilite  # Code highlighting
    - footnotes  # documentation footnotes
    - toc:  # table of contents, anchors
        permalink: true
    - pymdownx.betterem  # Better emphasis for italics, bold, etc
    - pymdownx.details  # Collapsible elements
    - pymdownx.emoji:  # Emoji support
        emoji_generator: '!!python/name:pymdownx.emoji.to_svg'  # Convert emojis to SVGs
    - pymdownx.inlinehilite  # inline code highlighting
    - pymdownx.keys  # Styled keyboard keys
    - pymdownx.magiclink  # auto-link various protocols and links
    - pymdownx.mark  # Inline highlighting/"marking"
    - pymdownx.snippets  # Inserting files into markdown
    - pymdownx.superfences  # Tabs for blocks
    - pymdownx.tasklist  # GFM style task lists
    - pymdownx.tilde  # delete and subscript tags
# Useful in the future?
#     https://pheasant.daizutabi.net/

plugins:
    - search

nav:
    - Overview: index.md
    - Setup: setup.md
    - Controllers:
        - OpenCM: controllers/opencm.md
        - OpenCR: controllers/opencr.md
        - U2D2: controllers/u2d2.md
        - Roll Your Own: controllers/ryo.md
    - Operation:
        - GUI Launcher: operation/launcher.md
        - ROS RVIZ: operation/rviz.md
        - MoveIt!: operation/moveit.md
        - Gazebo Simulation: operation/gazebo.md
        - Control GUI: operation/control_gui.md
        - Teleoperation: operation/teleop.md
        - CLI/Programming: operation/topics_and_services.md
    - Variants:
        - PincherX 100: pincherx_100.md
        - PincherX 150: pincherx_150.md
        - ReactorX 150: reactorx_150.md
        - ReactorX 200: reactorx_200.md
        - WidowX 200: widowx_200.md
        - WidowX 250: widowx_250.md
        - ViperX 250: viperx_250.md
        - ViperX 300: viperx_300.md
    - Example Demos:
        - MoveIt! States: demos/moveit-states.md
        - Pick and Place: demos/pick-and-place.md
    - Development:
        - Package Information: development/information.md
        - Using DMXL: development/dmxl.md
        - Communicating with Dynamixels: development/dynamixels.md
        - Contributing: development/contributing.md
        - Troubleshooting: development/troubleshooting.md
        - Resources: development/resources.md
    - FAQ: faq.md
    - About: about.md
    - Changelog: changelog.md

