# Contributing
!!! warning "WIP"
    This document is a Work In Progress

-----

Most packages and repositories associated with this project are located on GitLab: https://gitlab.com/dabit-industries/interbotix/x-series/  
If you would like to contribute in some way, please find the proper repository and read through the README.md.  

## Bug Reporting
Always provide the following information when submitting an issue or bug report:  
- Linux Distribution  
- ROS version  
- Steps to reproduce the problem

## Code or Document Contribution

### General Guide Lines
- It is better to be explicit than implicit
    - Longer variable names are OK
    - Document code accordingly
    - Write READMEs where a new user can understand the repo and code structure
- Try to match the style of the code, configuration, etc files that already exist
    - This ensures that styles match across files, and other developers can read your contributions

### Coding Style Guides
We try to follow these code style guides:  
- **C/C++** - [Linux Coding Style](https://www.kernel.org/doc/Documentation/process/coding-style.rst)  
- **Python** - [Python-Guide Coding Style (Pep8)](https://docs.python-guide.org/writing/style/)  
- **ROS** - [ROS Style Guide](https://wiki.ros.org/StyleGuide#ROS), [Interbotix Package Style Guide](../development/package-style-guide.md)

### Configuration Style Guides
At this time we do not have configuration guidelines.

### Documentation Style Guides
At this time we do not have documentation guidelines.

## Editorconfig
A [.editorconfig](https://editorconfig.org/) is provided in each repository for convinience in regards to style.

[gitlab]: https://gitlab.com/dabit-industries/interbotix_x-series/
