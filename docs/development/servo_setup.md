# Servo Setup
Best practices for your servo configuration

-----

## DMXL
Coming soon! [dxml][dmxl] is a tool to help manage Dynamixel controllers and servos.  
[dmxl][dmxl] can upload firmware, setup udev rules, 

## Dynamixel Workbench

### Install
Coming soon!

## Servo Setup
Look at [x-series/dual_servos](x-series/dual_servos.md) for information about arms with dual-servo joints.  

### Servo IDs
| ---------------- | --- |
| Joint            | ID  |
| ---------------- | --- |
| base             | 1   |
| shoulder         | 2   |
| elbow            | 3   |
| wrist_pitch      | 4   |
| wrist_roll       | 5   |
| gripper          | 6   |
| ...              | ... |
| [shadow][shadow] | 20X |
| ---------------- | --- |

[dxml]: https://git.sr.ht/~lucidone/dmxl
[shadow]: x-series/dual_servos.md
