# Frequently Asked Questions
...or common problems we have run into that you might be having as well.

-----

This page aims to shed some light on problems you may encounter while working with the X-Series suite.  
Most issues that arise from system and connetion problems should have explanitory error messages.  
Tools like [DMXL](../dmxl.md) aid in setting up and diagnosing systems.

## Usage
---
**Q.** How do I view and/or report issues with provided software and documentation?  
**A.** You can [view open issues on GitLab](https://gitlab.com/groups/dabit-industries/interbotix/x-series/-/issues) as well as [select a project](https://gitlab.com/dabit-industries/interbotix/x-series) and create an issue. Please be descriptive, and provide any [debugging and diagnostics](../development/debugging.md) information.
---

## DMXL


## ROS Packages


## Open Manipulator Controller
---
**Q.** The Controller does not start, and has an error "[RxPacketError]" and "Please check your Dynamixel ID".  
**A.** This can be caused by a few reasons. One reason is that the controller is not plugged in, or that the servos are not powered on. Check the cable connections. If you have, head over to [Debugging USB Connections](#TODO) and try the solutions there.  
---
**Q.** "[ERROR] [DynamixelWorkbench] Failed to set Current Based Position Control Mode!"  
**A.** This is a known error. It is currently being evaluated. [//](#TODO: create issue on gitlab?)
---
**Q.** "[ERROR] [TxRxResult] Incorrect status packet!" "[ERROR] groupSyncRead getdata failed"  
**A.** These are known errors, and seem to be due to packet loss from the OpenCM9.04C Serial Passthrough. Find the [issue on GitLab](#TODO)
---
